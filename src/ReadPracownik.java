import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ReadPracownik {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        try(FileInputStream fis=new FileInputStream("pracownicy.dat");
        		Scanner sc=new Scanner(fis)){
        	while(sc.hasNextLine()){
        		StringTokenizer st=new StringTokenizer(sc.nextLine());
        		System.out.println(st.nextToken()+" "+st.nextToken()+" "+
        		st.nextToken()+" "+st.nextToken());
        	}
        	
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
