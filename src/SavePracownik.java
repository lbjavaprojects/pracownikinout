import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SavePracownik {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       List<Pracownik> l=new LinkedList<Pracownik>();
       l.add(new Pracownik("Jan", "Kowalski", LocalDate.of(1990, 1, 11), 
    		   LocalDate.of(2017, 11, 12)));
       l.add(new Pracownik("Tomasz", "Mleczko", LocalDate.of(1980, 11, 13), 
    		   LocalDate.of(2016, 1, 13)));
       try(FileOutputStream fos=new FileOutputStream("pracownicy.dat");
    		   PrintWriter pw=new PrintWriter(fos)){
    	   Iterator<Pracownik> it=l.iterator();
    	   while(it.hasNext()){
    		   Pracownik tmp=it.next();
    		   pw.println(tmp.getImie()+" "+tmp.getNazwisko()+" "+tmp.getDataUr().toString()+
    				   " "+tmp.getDataZatr().toString());
    	   }
    	   System.out.println("Lista pracowników zapisana.");
    	   
       } catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

}
