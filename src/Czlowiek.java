import java.time.LocalDate;
import java.time.LocalTime;

public abstract class Czlowiek {
	private String imie, nazwisko;
	private LocalDate dataUr;
	public String getImie() {
		return imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public LocalDate getDataUr() {
		return dataUr;
	}
	public Czlowiek(String imie, String nazwisko, LocalDate dataUr) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.dataUr = dataUr;
	}
	
	
}
